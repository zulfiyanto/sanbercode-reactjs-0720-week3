import React from "react";

import "./app.css";

class Nama extends React.Component {
  render() {
    return <p>{this.props.nama}</p>;
  }
}

class Harga extends React.Component {
  render() {
    return <p>{this.props.harga}</p>;
  }
}

class Berat extends React.Component {
  render() {
    return <p>{this.props.berat} Kg</p>;
  }
}

class Table1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataHargaBuah: [
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 },
      ],
      input: {
        nama: "",
        harga: "",
        berat: "",
      },
      indexOfForm: -1,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleChange(event) {
    let input = { ...this.state.input };
    input[event.target.name] = event.target.value;
    this.setState({
      input,
    });
  }

  handleEdit(event) {
    let index = event.target.value;
    let list = this.state.dataHargaBuah[index];
    this.setState({
      input: {
        nama: list.nama,
        harga: list.harga,
        berat: list.berat,
      },
      indexOfForm: index,
    });
  }

  handleDelete(event) {
    let index = event.target.value;
    let newDaftar = this.state.dataHargaBuah;
    let editDaftar = newDaftar[this.state.indexOfForm];
    newDaftar.splice(index, 1);

    if (editDaftar !== undefined) {
      let newIndex = newDaftar.findIndex((item) => item === editDaftar);
      this.setState({ dataHargaBuah: newDaftar, indexOfForm: newIndex });
    } else {
      this.setState({ dataHargaBuah: newDaftar });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    let input = this.state.input;
    if (
      input["nama"].replace(/\s/g, "") !== "" &&
      input["harga"].toString().replace(/\s/g, "") !== "" &&
      input["berat"].toString().replace(/\s/g, "") !== ""
    );
    let newDaftar = this.state.dataHargaBuah;
    let index = this.state.indexOfForm;
    console.log(index);
    if (index === -1) {
      newDaftar = [...newDaftar, input];
    } else {
      newDaftar[index] = input;
    }

    this.setState({
      hargaBuah: newDaftar,
      input: {
        nama: "",
        harga: "",
        berat: "",
      },
      indexOfForm: -1,
    });
  }

  render() {
    return (
      <div className="container">
        <h1>Tabel Harga Buah</h1>
        <table>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Action</th>
          </tr>
          {this.state.dataHargaBuah.map((post, index) => {
            return (
              <tr key={index}>
                <td>
                  <Nama nama={post.nama} />
                </td>
                <td>
                  <Harga harga={post.harga} />
                </td>
                <td>
                  <Berat berat={post.berat / 1000} />
                </td>
                <td className="action">
                  <button onClick={this.handleEdit} value={index}>
                    Edit
                  </button>
                  <button onClick={this.handleDelete} value={index}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </table>

        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            name="nama"
            value={this.state.input.nama}
            onChange={this.handleChange}
            placeholder="Nama"
          />
          <input
            type="text"
            name="harga"
            value={this.state.input.harga}
            onChange={this.handleChange}
            placeholder="Harga"
          />
          <input
            type="text"
            name="berat"
            value={this.state.input.berat}
            onChange={this.handleChange}
            placeholder="Berat"
          />
          <button>submit</button>
        </form>
      </div>
    );
  }
}

export default Table1;
