import React, { useContext } from "react";
import { ApiContext } from "./ApiContext";
import axios from "axios";

class Row extends React.Component {
  render() {
    return <td>{this.props.a}</td>;
  }
}

const ApiList = () => {
  const [dataHarga, setDataHarga] = useContext(ApiContext);
  const [setInput] = useContext(ApiContext);
  const [setIndexForm] = useContext(ApiContext);
  const [setIdFruit] = useContext(ApiContext);

  const handleDelete = (event) => {
    let id = parseInt(event.target.value);
    let newDataHarga = dataHarga.filter((item) => item.id !== id);
    axios
      .delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
      .then((res) => {
        console.log(res);
      });

    setDataHarga([...newDataHarga]);
  };

  const handleEdit = (event) => {
    let id = parseInt(event.target.value);
    let buah = dataHarga.find((item) => item.id === id);
    setInput({ name: buah.name, price: buah.price, weight: buah.weight });
    setIdFruit(id);
    setIndexForm("edit");
  };

  return (
    <table>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Harga</th>
        <th>Berat</th>
        <th>Action</th>
      </tr>
      {dataHarga !== null &&
        dataHarga.map((item, index) => {
          return (
            <tr key={index}>
              <Row a={index + 1} />
              <Row a={item.name} />
              <Row a={item.price} />
              <Row a={item.weight / 1000 + "Kg"} />
              <td className="action">
                <button onClick={handleEdit} value={item.id}>
                  Edit
                </button>
                <button onClick={handleDelete} value={item.id}>
                  Delete
                </button>
              </td>
            </tr>
          );
        })}
    </table>
  );
};

export default ApiList;
