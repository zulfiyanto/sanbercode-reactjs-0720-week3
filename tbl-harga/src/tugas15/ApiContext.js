import React, { useState, createContext } from "react";
import "./app.css";

export const ApiContext = createContext;

const ApiProvider = (props) => {
  const [dataHarga, setDataHarga] = useState(null);
  const [input, setInput] = useState({
    name: "",
    price: "",
    weight: 0,
  });
  const [idFruit, setIdFruit] = useState(0);
  const [indexForm, setIndexForm] = useState("create");

  return (
    <ApiContext.Provider
      value={
        ([dataHarga, setDataHarga],
        [idFruit, setIdFruit],
        [indexForm, setIndexForm],
        [input, setInput])
      }
    >
      {props.children}
    </ApiContext.Provider>
  );
};

export default ApiProvider;
