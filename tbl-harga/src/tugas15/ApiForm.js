import React, { useContext, useEffect } from "react";
import { ApiContext } from "./ApiContext";
import axios from "axios";

const ApiForm = () => {
  const [dataHarga, setDataHarga] = useContext(ApiContext);
  const [input, setInput] = useContext(ApiContext);
  const [idFruit, setIdFruit] = useContext(ApiContext);
  const [indexForm, setIndexForm] = useContext(ApiContext);

  useEffect(() => {
    if (dataHarga.length === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          setDataHarga(
            res.data.map((item) => {
              return {
                id: item.id,
                name: item.name,
                price: item.price,
                weight: item.weight,
              };
            })
          );
        });
    }
  }, [dataHarga]);

  const handleChange = (event) => {
    let typeOfInput = event.target.name;

    switch (typeOfInput) {
      case "name": {
        setInput({ ...input, name: event.target.value });
        break;
      }
      case "price": {
        setInput({ ...input, price: event.target.value });
        break;
      }
      case "weight": {
        setInput({ ...input, weight: event.target.value });
        break;
      }
      default: {
        break;
      }
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let name = input.name;
    let price = input.price;
    let weight = input.weight;

    if (
      name.replace(/\s/g, "") !== "" &&
      price.toString().replace(/\s/g, "") !== "" &&
      weight.toString().replace(/\s/g, "") !== ""
    ) {
      if (indexForm === "create") {
        axios
          .post(`http://backendexample.sanbercloud.com/api/fruits`, input)
          .then((res) => {
            setDataHarga([
              ...dataHarga,
              {
                id: res.data.id,
                nama: input.name,
                harga: input.price,
                berat: input.weight,
              },
            ]);
          });
      } else if (indexForm === "edit") {
        axios
          .put(
            `http://backendexample.sanbercloud.com/api/fruits/${idFruit}`,
            input
          )
          .then((res) => {
            let dataBuah = dataHarga.find((item) => item.id === idFruit);
            dataBuah.name = input.name;
            dataBuah.price = input.price;
            dataBuah.weight = input.weight;
            setDataHarga([...dataHarga]);
          });
      }
      setIndexForm("create");
      setIdFruit(0);
      setInput({
        name: "",
        price: "",
        weight: 0,
      });
    }
  };

  return (
    <div className="container">
      <h1>Tabel Harga Buah</h1>

      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="name"
          value={input.name}
          onChange={handleChange}
          placeholder="Nama"
        />
        <input
          type="text"
          name="price"
          value={input.price}
          onChange={handleChange}
          placeholder="Harga"
        />
        <input
          type="text"
          name="weight"
          value={input.weight}
          onChange={handleChange}
          placeholder="Berat"
        />
        <button>submit</button>
      </form>
    </div>
  );
};

export default ApiForm;
