import React, { Component } from "react";
import Clock from "react-live-clock";

class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 100,
    };
  }

  componentDidUpdate() {}

  componentDidMount() {
    if (this.props.start !== undefined) {
      this.setState({ time: this.props.start });
    }
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
    });
  }

  render() {
    return (
      <>
        {this.state.time >= 0 && (
          <div style={{ display: "flex" }}>
            <h1 style={{ flex: 1 }}>
              <Clock
                format={"HH:mm:ss"}
                ticking={true}
                timezone={"indonesia"}
              />
            </h1>
            <h1 style={{ textAlign: "center", flex: 1 }}>{this.state.time}</h1>{" "}
          </div>
        )}
      </>
    );
  }
}

export default Timer;
