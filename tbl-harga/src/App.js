import React from "react";
import "./App.css";
import { ApiProvider } from "./tugas15/ApiContext";
import ApiList from "./tugas15/ApiList";
import ApiForm from "./tugas15/ApiForm";
// import Timer from "./tugas12/app.js";

function App() {
  return (
    <ApiProvider>
      <ApiList />
      <ApiForm />
    </ApiProvider>
  );
}

export default App;
