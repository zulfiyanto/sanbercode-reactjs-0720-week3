import React, { useState, useEffect } from "react";
import "./app.css";
import axios from "axios";

class Row extends React.Component {
  render() {
    return <td>{this.props.a}</td>;
  }
}

const Api = () => {
  const [dataHarga, setDataHarga] = useState(null);
  const [input, setInput] = useState({
    name: "",
    price: "",
    weight: 0,
  });
  const [idFruit, setIdFruit] = useState(0);
  const [indexForm, setIndexForm] = useState("create");

  useEffect(() => {
    if (dataHarga === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          setDataHarga(
            res.data.map((item) => {
              return {
                id: item.id,
                name: item.name,
                price: item.price,
                weight: item.weight,
              };
            })
          );
        });
    }
  }, [dataHarga]);

  const handleDelete = (event) => {
    let id = parseInt(event.target.value);
    let newDataHarga = dataHarga.filter((item) => item.id !== id);
    axios
      .delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
      .then((res) => {
        console.log(res);
      });

    setDataHarga([...newDataHarga]);
  };

  const handleEdit = (event) => {
    let id = parseInt(event.target.value);
    let buah = dataHarga.find((item) => item.id === id);
    setInput({ name: buah.name, price: buah.price, weight: buah.weight });
    setIdFruit(id);
    setIndexForm("edit");
  };

  const handleChange = (event) => {
    let typeOfInput = event.target.name;

    switch (typeOfInput) {
      case "name": {
        setInput({ ...input, name: event.target.value });
        break;
      }
      case "price": {
        setInput({ ...input, price: event.target.value });
        break;
      }
      case "weight": {
        setInput({ ...input, weight: event.target.value });
        break;
      }
      default: {
        break;
      }
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let name = input.name;
    let price = input.price;
    let weight = input.weight;

    if (
      name.replace(/\s/g, "") !== "" &&
      price.toString().replace(/\s/g, "") !== "" &&
      weight.toString().replace(/\s/g, "") !== ""
    ) {
      if (indexForm === "create") {
        axios
          .post(`http://backendexample.sanbercloud.com/api/fruits`, input)
          .then((res) => {
            setDataHarga([
              ...dataHarga,
              {
                id: res.data.id,
                nama: input.name,
                harga: input.price,
                berat: input.weight,
              },
            ]);
          });
      } else if (indexForm === "edit") {
        axios
          .put(
            `http://backendexample.sanbercloud.com/api/fruits/${idFruit}`,
            input
          )
          .then((res) => {
            let dataBuah = dataHarga.find((item) => item.id === idFruit);
            dataBuah.name = input.name;
            dataBuah.price = input.price;
            dataBuah.weight = input.weight;
            setDataHarga([...dataHarga]);
          });
      }
      setIndexForm("create");
      setIdFruit(0);
      setInput({
        name: "",
        price: "",
        weight: 0,
      });
    }
  };

  return (
    <div className="container">
      <h1>Tabel Harga Buah</h1>
      <table>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Harga</th>
          <th>Berat</th>
          <th>Action</th>
        </tr>
        {dataHarga !== null &&
          dataHarga.map((item, index) => {
            return (
              <tr key={index}>
                <Row a={index + 1} />
                <Row a={item.name} />
                <Row a={item.price} />
                <Row a={item.weight / 1000 + "Kg"} />
                <td className="action">
                  <button onClick={handleEdit} value={item.id}>
                    Edit
                  </button>
                  <button onClick={handleDelete} value={item.id}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
      </table>

      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="name"
          value={input.name}
          onChange={handleChange}
          placeholder="Nama"
        />
        <input
          type="text"
          name="price"
          value={input.price}
          onChange={handleChange}
          placeholder="Harga"
        />
        <input
          type="text"
          name="weight"
          value={input.weight}
          onChange={handleChange}
          placeholder="Berat"
        />
        <button>submit</button>
      </form>
    </div>
  );
};

export default Api;
